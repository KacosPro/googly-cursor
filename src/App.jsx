import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.catchMouseMove = this.catchMouseMove.bind(this);
  }

  catchMouseMove(event) {
    const { pageX, pageY } = event;

    this.cursor.style.left = `${pageX - 5}px`;
    this.cursor.style.top = `${pageY - 5}px`;
    setTimeout(() => {
      this.cursorContainerInner.style.left = `${pageX - 10}px`;
      this.cursorContainerInner.style.top = `${pageY - 10}px`;
    }, 100);
    setTimeout(() => {
      this.cursorContainerOutter.style.left = `${pageX - 15}px`;
      this.cursorContainerOutter.style.top = `${pageY - 15}px`;
    }, 200);
  }

  render() {
    return (
      <div onMouseMove={this.catchMouseMove} className="app">
        <div ref={(el) => { this.cursorContainerOutter = el; }} className="cursor-container_outter">
          <div ref={(el) => { this.cursorContainerInner = el; }} className="cursor-container_inner">
            <div ref={(el) => { this.cursor = el; }} className="cursor" />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
